import java.util.*;

public class Barman extends Thread
{
    private String name;
    private String cocktailName;
    private List<Ingredient> ingredients = new ArrayList<Ingredient>();

    public Barman(String name, String cocktailName, List<Ingredient> ingredients) {
        this.name = name;
        this.cocktailName = cocktailName;
        this.ingredients = ingredients;
    }

    @Override
    public void run() {
        int i = 1;

        while (true) {
            try{
                
                List<Ingredient> tempIngredients = new ArrayList<Ingredient>();

                try {
                    for (Ingredient ingredient : ingredients) {
                        ingredient.take(name);
                        tempIngredients.add(ingredient);
                        
                    }
                } catch (InterruptedException e) {
                    for (Ingredient ingredient: tempIngredients) {
                        ingredient.release(name, true);
                    }
                    continue;
                }

                System.out.println(String.format("\n %s : Prépare le cocktail n° %d : %s", name, i, cocktailName));

                Thread.sleep(2000);

                System.out.println(String.format("\n %s : A fini le cocktail n° %d : %s", name, i, cocktailName));

                for (Ingredient ingredient : ingredients) {
                    ingredient.release(name, false);
                }

                i++;
            } catch (Exception e) {
                System.out.println("Error");
            }
        }
    }
}