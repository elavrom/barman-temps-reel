
public class Ingredient {

	private String name;
	private int sem = 1;

	// Constructor
	public Ingredient(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// synchronized : évite l'interruption par un autre thread
	public synchronized void take(String barmanName) throws InterruptedException
	{
		if(sem == 0)
		{
			wait(3000); // attente 3 secondes puis vérification si le sem a changé, sinon on throw une interruptedexception pour release les ingrédients pris
            if(sem == 0){
                throw new InterruptedException();
            }
		}
		sem = sem - 1;
        System.out.println(String.format("\n %s : Prend %s", barmanName, name));
	}

	public synchronized void release(String barmanName, Boolean isException) 
	{
		sem = sem + 1;
        if (isException) {
            System.out.println(String.format("\n %s : N'a pas pu préparer son cocktail. Repose %s", barmanName, name));

        } else {
            System.out.println(String.format("\n %s : Repose %s", barmanName, name));
        }
		notify(); // Réveille le premier thread bloqué sur wait
	}
}
