import java.util.*;

class Main {
    public static void main(String[] args) throws InterruptedException{
        Ingredient menthe = new Ingredient("Menthe");
        Ingredient glace = new Ingredient("Glace pilée");
        Ingredient citronVert = new Ingredient("Citron Vert");
        Ingredient limonade = new Ingredient("Limonade");
        Ingredient orange = new Ingredient("Orange");
        Ingredient grenadine = new Ingredient("Grenadine");

        List<Ingredient>virginMojito = new ArrayList<Ingredient>();
        Collections.addAll(virginMojito, menthe, glace, citronVert, limonade);

        List<Ingredient> pinaColada = new ArrayList<Ingredient>();
        Collections.addAll(pinaColada, orange, grenadine, glace, menthe);
       

        Barman romain = new Barman("Romain", "Virgin Mojito", virginMojito);
        
        Barman sebastien = new Barman("Sébastien", "Virgin Pina colada", pinaColada);
      
        romain.start();
        sebastien.start();
    }
}